// HarmancCourse_004_Interfaces.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>


int
x = 10;

void funcion() {
    int x = 5;
    std::cout << "x local: " << x << std::endl;
    std::cout << "x global: " << ::x << std::endl;

}

class IFigura {
public:
    virtual void dibujar() const = 0;
    virtual ~IFigura() {}
};

class Circulo : public IFigura {
public:
    void dibujar() const override {
        std::cout << "Dibujo de un circulo.." << std::endl;
    }

};

class Cuadrado : public IFigura {
public:
    void dibujar() const override {
        std::cout << "Dibujo de un cuadrado..." << std::endl;
    }
};

int suma(int a, int b) {
    return a + b;
}

double suma(double a, double b)
{
    return a + b;
}

int suma(int a, int b, int c)
{
    return a + b + c;
}

class Complejo {
private:
    double real;
    double imaginario;

public:
    Complejo (double r = 0.0, double i = 0.0) : real(r), imaginario(i) {}

    // Sobrecarga del operador +
    Complejo operator+(const Complejo& otro) const {
        return Complejo(real + otro.real, imaginario + otro.imaginario);
    }
    friend std::ostream& operator << (std::ostream& out, const Complejo& c) {
        out << "(" << c.real << " + " << c.imaginario << "i)";
        return out;
    }

    // Sobrecarga del operador ==


};

namespace MiEmpresa {

    namespace ProtectoA {
        void funcion() {
            std::cout << "Proyecto A" << std::endl;
        }
    }
    namespace ProtectoB {
        void funcion() {
            std::cout << "Proyecto B" << std::endl;
        }
    }

    void funcion() {
        std::cout << "Funcion" << std::endl;
    }
}

int maximo(int a, int b)
{
    if (a > b) {
        return a;
    }
    else {
        return b;
    }
}

double maximo(double a, double b)
{
    if (a > b) {
        return a;
    }
    else {
        return b;
    }
}

char maximo(char a, char b)
{
    if (a > b) {
        return a;
    }
    else {
        return b;
    }
}


class Vector2D {
private:
    double x, y;
public:
    //Constructor
    Vector2D(double _x = 0.0, double _y = 0.0) : x(_x), y(_y) {}



    //Sobrecarga del operador +
    Vector2D operator+(const Vector2D& v) const {
        return Vector2D(x + v.x, y + v.y);
    }

    //Sobrecarga del operador -
    Vector2D operator-(const Vector2D& v) const {
        return Vector2D(x - v.x, y - v.y);
    }

    void mostrar() const {
        std::cout << "(" << x << "," << y << ")" << std::endl;
    }
};

namespace ProjA = MiEmpresa::ProtectoA;
namespace ProjB = MiEmpresa::ProtectoB;

int main()
{
    std::cout << "Hello World!\n";
    IFigura* figura1 = new Circulo();
    IFigura* figura2 = new Cuadrado();

    figura1->dibujar();
    figura2->dibujar();

    delete figura1;
    delete figura2;

    int resultado1 = suma(5, 9); //Llamar a suma (int, int)
    double resultado2 = suma(2.5, 4.9); //Llamar a suma (double, double)
    int resultado3 = suma(1, 2, 3);

    std::cout << "Resultado 1: " << resultado1 << std::endl;
    std::cout << "Resultado 2: " << resultado2 << std::endl;
    std::cout << "Resultado 3: " << resultado3 << std::endl;


    Complejo c1(3.0, 4.0);
    Complejo c2(1.0, 2.0);
    Complejo c3 = c1 + c2;
    
    std::cout << "c1: " << c1 << std::endl;
    std::cout << "c2: " << c2 << std::endl;
    std::cout << "c3 (c1 + c2): " << c3 << std::endl;

    //Namespaces & Scope

    //using namespace MiEmpresa::ProtectoA;
    //using namespace MiEmpresa::ProtectoB;
    setlocale(LC_CTYPE, "Spanish");

    MiEmpresa::funcion();
    //MiEmpresa::ProtectoA::funcion();
    //MiEmpresa::ProtectoA::funcion();

    ProjA::funcion();
    ProjB::funcion();

    //Scope
    funcion();



    //Ejercicio 1
    std::cout << "Maximo entre 3 y 7: " << maximo(3, 7) << std::endl;
    std::cout << "Maximo entre 2.5 y 1.5: " << maximo(2.5, 1.5) << std::endl;
    std::cout << "Maximo entre 'a' y 'z': " << maximo('a', 'z') << std::endl;

    //Ejercicio 2
    Vector2D v1(2.0, 3.0);
    Vector2D v2(1.0, -1.0);

    Vector2D suma = v1 + v2;
    Vector2D resta = v1 - v2;

    std::cout << "v1 + v2 = ";
    suma.mostrar();
    std::cout << "v1 - v2 = ";
    resta.mostrar();


    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
