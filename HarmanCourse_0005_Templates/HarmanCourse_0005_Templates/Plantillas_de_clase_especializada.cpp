//#include <iostream>
//
//template <typename Tb>
//class Almacen {
//private:
//    Tb valor;
//
//public:
//    Almacen(Tb v) : valor (v) {}
//
//    Tb obtenerValor()
//    {
//        return valor;
//    }
//
//    void establecerValor(Tb v) {
//        valor = v;
//    }
//
//};
//
//
//template <>
//class Almacen<bool> {
//private:
//	unsigned char valor;
//public:
//	Almacen(bool v) : valor(v) {}
//	bool obtenerValor() { return valor & 0x01; }
//	void establecerValor(bool v) { valor = v ? 1 : 0; }
//};
////std::string maximo<std::string>(std::string a, std::string b)
////{
////	std::cout << "usando plantilla especializada para std::string" << std::endl;
////	return (a.length() > b.length()) ? a : b;
////}
//
//int main() {
//	setlocale(LC_CTYPE, "Spanish");
//	Almacen<int> entero(10);
//	Almacen<bool> booleano(true);
//
//	std::cout << "Entero: " << entero.obtenerValor() << std::endl;
//	std::cout << "Booleano: " << booleano.obtenerValor() << std::endl;
//
//	entero.establecerValor(20);
//	booleano.establecerValor(false);
//
//	std::cout << "Entero: " << entero.obtenerValor() << std::endl;
//	std::cout << "Booleano: " << booleano.obtenerValor() << std::endl;
//
//	return 0;
//}