#include <iostream>

template<typename T>
void intercambiar(T * a, T * b)
{
	T temp_a = *a;
	T temp_b = *b;
	*a = temp_b;
	*b = temp_a;
}


int main() {
	setlocale(LC_CTYPE, "Spanish");
	int x = 5, y = 10;

	intercambiar(&x, &y);

	std::cout << "x: " << x << ", y: " << y << std::endl;

	double a = 1.5, b = 3.5;
	intercambiar(&a, &b);

	std::cout << "a: " << a << ", b: " << b << std::endl;

	return 0;
}