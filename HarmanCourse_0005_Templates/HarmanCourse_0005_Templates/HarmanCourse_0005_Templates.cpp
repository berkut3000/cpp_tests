//// HarmanCourse_0005_Templates.cpp : This file contains the 'main' function. Program execution begins and ends there.
////
//
//#include <iostream>
//
//template <typename T>
//T maximo(T a, T b) {
//    return (a > b) ? a : b;
//}
//
//template <typename Tb>
//class Almacen {
//private:
//    Tb valor;
//
//public:
//    Almacen(Tb v) : valor (v) {}
//
//    Tb obtenerValor()
//    {
//        return valor;
//    }
//
//    void establecerValor(Tb v) {
//        valor = v;
//    }
//
//};
//
//int main()
//{
//    setlocale(LC_CTYPE, "Spanish");
//    int x = 10, y = 20;
//    double a = 5.5, b = 2.4;
//    char c1 = 'a', c2 = 'x';
//
//    std::cout << "El m�ximo entre " << x << " y " << y << " es " << maximo(x,y) << std::endl;
//    std::cout << "El m�ximo entre " << a << " y " << a << " es " << maximo(a, b) << std::endl;
//    std::cout << "El m�ximo entre " << c1 << " y " << c2 << " es " << maximo(c1, c2) << std::endl;
//
//    Almacen<int> entero(10);
//    Almacen<double> decimal(5.5);
//    Almacen<std::string> cadena("hola");
//
//    std::cout << "Entero: " << entero.obtenerValor() << std::endl;
//    std::cout << "Decimal: " << decimal.obtenerValor() << std::endl;
//    std::cout << "Cadena: " << cadena.obtenerValor() << std::endl;
//
//}
//
//// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
//// Debug program: F5 or Debug > Start Debugging menu
//
//// Tips for Getting Started: 
////   1. Use the Solution Explorer window to add/manage files
////   2. Use the Team Explorer window to connect to source control
////   3. Use the Output window to see build output and other messages
////   4. Use the Error List window to view errors
////   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
////   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
