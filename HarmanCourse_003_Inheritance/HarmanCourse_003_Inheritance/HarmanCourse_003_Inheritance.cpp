// HarmanCourse_003_Inheritance.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#define _USE_MATH_DEFINES // for C++
#include <iostream>
#include <vector>
#include <cmath>

class Vehiculo {
public:
    void encenderMotor() {
        std::cout << "Motor encendido." << std::endl;
    }
};

class Coche : public Vehiculo {
public:
    void tocarBocina() {
        std::cout << "Tocando bocina" << std::endl;
    }

};

//Clasebase
class VehiculoTerrestre {
public:
    void conducir() {
        std::cout << "Navegando en Agua " << std::endl;
    }
};

class VehiculoMaritimo {
public:
    void navegar() {
        std::cout << "Navegando en agua" << std::endl;
    }
};

class Anfibio : public VehiculoTerrestre, public VehiculoMaritimo {
public:
    void transformarse() {
        std::cout << "Transformacion para cambiar de modo" << std::endl;
    }
};


class Persona {
protected:
    std::string nombre;
    int edad;

public:
    Persona(std::string n, int e) : nombre(n), edad (e) {}

    void mostrarInformacion() {
        std::cout << "Nombre: " << nombre << ", Edad: " << edad << std::endl;
    }
};


class Estudiante : public Persona {
protected:
    std::string carrera;

public:
    Estudiante(std::string n, int e, std::string c) : Persona(n, e), carrera(c) {}

    void mostrarInformacion() {
        Persona::mostrarInformacion();
        std::cout << "Carrera: " << carrera << std::endl;
    }
};


class Animal {
public:
     virtual void hacerSonido() { //Indica que una funcion miembro puede ser sobreescrita en una clase derivada.
        std::cout << "El Animal hace un sonido. " << std::endl;
    }
};

class Perro : public Animal {
public:
     void hacerSonido() override { // Override indica que la funcion sobreescribira una funcion del padre
        std::cout << "El perro ladra." << std::endl;
    }
};

class Gato : public Animal {
public:
    void hacerSonido() override {
        std::cout << "El gato maulla." << std::endl;
    }
};


class Figura {
public:
    virtual double calcularArea() = 0; // Funcion virtual pura
};


class Circulo : public Figura {
private:
    double radio;
public:
    Circulo(double r) : radio(r) {}

    double calcularArea() override {
        return M_PI * radio * radio;
    }
};


class Rectangulo : public Figura {
private:
    double ancho, alto;
public:
    Rectangulo(double a, double h) : ancho(a), alto (h) {}

    double calcularArea() override {
        return ancho * alto;
    }
};

int main()
{
    Coche miCoche;
    miCoche.encenderMotor(); // Metodo Heredado de vehiculo
    miCoche.tocarBocina();

    //Herencia Multiple 

    Anfibio miAnfibio;
    miAnfibio.conducir();
    miAnfibio.navegar();
    miAnfibio.transformarse();

    //
    setlocale( LC_CTYPE , "Spanish");
    Estudiante estudiante1("Laura", 22, "Ingenierķa");
    estudiante1.mostrarInformacion();

    std::vector<Animal*> animales;
    animales.push_back(new Perro());
    animales.push_back(new Gato());
    animales.push_back(new Animal());

    for (size_t i = 0; i < animales.size(); ++i) {
        animales[i]->hacerSonido();
    }

    for (size_t i = 0; i < animales.size(); ++i) {
        delete animales[i];
    }

    Animal* animal = new Perro();
    animal->hacerSonido();
    //animal.hacerSonido();

    delete animal;

    Figura* f1 = new Circulo(5);
    Figura* f2 = new Rectangulo(4, 6);

    std::cout << "Area del circulo es: " << f1->calcularArea() << std::endl;
    std::cout << "Area del Rectangulo es: " << f2->calcularArea() << std::endl;


    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
