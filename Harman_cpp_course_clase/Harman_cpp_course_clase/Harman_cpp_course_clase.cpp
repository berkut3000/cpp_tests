// Harman_cpp_course_clase.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Persona {
public:
    std::string nombre;
    int edad;

    Persona(std::string n, int e) : nombre (n), edad (e) {
        //nombre = n;
        //edad = e;
        std::cout << "Constructor llamado para " << nombre << "con edad " << edad << std::endl;
    }

    ~Persona() {
        std::cout << "Deconstructor llamado para " << nombre << std::endl;
    }

    void mostrar_informacion() {
        std::cout << "Nombre: " << nombre << ", edad: " << edad << std::endl;
    }
};


class CuentaBancaria {
protected:
    double saldo = 0;
public:
    void depositar(double cantidad)
    {
        saldo += cantidad;
    }

    void retirar(double cantidad)
    {
        if (saldo < cantidad)
        {
            std::cout << "Fondos insuficientes" << std::endl;
        }
        else {
            saldo -= cantidad;
        }
    }


    double obternerSaldo()
    {
        return saldo;
    }

};

class CuentaAhorros : public CuentaBancaria {
public:
    void agregarInteres(double tasa) {
        saldo += saldo * tasa;
    }
};


int main()
{
    Persona persona1("Antonio", 31);
    Persona persona2("Luis", 23);
    //persona1.mostrar_informacion;
    //std::cout << "Clases\n";
    //Persona persona1;
    //persona1.nombre = "Antonio";
    //persona1.edad = 31;

    persona1.mostrar_informacion();

    CuentaBancaria cuenta_bancaria1;
    std::cout << cuenta_bancaria1.obternerSaldo() << std::endl;
    cuenta_bancaria1.depositar(500);
    std::cout << cuenta_bancaria1.obternerSaldo() << std::endl;
    cuenta_bancaria1.retirar(501);
    std::cout << cuenta_bancaria1.obternerSaldo() << std::endl;

    CuentaAhorros cuenta_ahorros1;
    cuenta_ahorros1.depositar(500);

    std::cout << cuenta_ahorros1.obternerSaldo() << std::endl;
    cuenta_ahorros1.agregarInteres(0.12);
    std::cout << cuenta_ahorros1.obternerSaldo() << std::endl;


    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
