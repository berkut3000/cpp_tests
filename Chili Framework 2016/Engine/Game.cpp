/****************************************************************************************** 
 *	Chili DirectX Framework Version 16.07.20											  *	
 *	Game.cpp																			  *
 *	Copyright 2016 PlanetChili.net <http://www.planetchili.net>							  *
 *																						  *
 *	This file is part of The Chili DirectX Framework.									  *
 *																						  *
 *	The Chili DirectX Framework is free software: you can redistribute it and/or modify	  *
 *	it under the terms of the GNU General Public License as published by				  *
 *	the Free Software Foundation, either version 3 of the License, or					  *
 *	(at your option) any later version.													  *
 *																						  *
 *	The Chili DirectX Framework is distributed in the hope that it will be useful,		  *
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of						  *
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the						  *
 *	GNU General Public License for more details.										  *
 *																						  *
 *	You should have received a copy of the GNU General Public License					  *
 *	along with The Chili DirectX Framework.  If not, see <http://www.gnu.org/licenses/>.  *
 ******************************************************************************************/
#include "MainWindow.h"
#include "Game.h"

Game::Game( MainWindow& wnd )
	:
	wnd( wnd ),
	gfx( wnd )
{
}

void Game::Go()
{
	gfx.BeginFrame();	
	UpdateModel();
	ComposeFrame();
	gfx.EndFrame();
}

void Game::UpdateModel()
{
	color_r = 127;
	color_g = 127;
	color_b = 255;
	if (TRUE == wnd.kbd.KeyIsPressed(VK_UP))
	{
		g_y_pos -= g_pos_multiplier;
	}
	if (TRUE == wnd.kbd.KeyIsPressed(VK_DOWN))
	{
		g_y_pos += g_pos_multiplier;
	}
	if (TRUE == wnd.kbd.KeyIsPressed(VK_LEFT))
	{
		g_x_pos -= g_pos_multiplier;
	}
	if (TRUE == wnd.kbd.KeyIsPressed(VK_RIGHT))
	{
		g_x_pos += g_pos_multiplier;
	}

	if (TRUE == wnd.kbd.KeyIsPressed(VK_NUMPAD8))
	{
		g_pos_multiplier++;
	}
	if (TRUE == wnd.kbd.KeyIsPressed(VK_NUMPAD2))
	{
		g_pos_multiplier--;
	}

	if (g_pos_multiplier < 0)
	{
		g_pos_multiplier = 0;
	}

	if (TRUE == wnd.kbd.KeyIsPressed(VK_NUMPAD1))
	{
		color_r = 60;
		color_g = 80;
		color_b = 90;
	}

#if 0
	g_x_pos += g_pos_multiplier;
	g_y_pos += g_pos_multiplier;
#endif

#if 1
	if (750 < g_x_pos)
	{
		g_x_pos = 50;
	}
	if (50 > g_x_pos)
	{
		g_x_pos = 750;
	}
#endif
#if 1
	if (550 < g_y_pos)
	{
		g_y_pos = 50;
	}
	if (50 > g_y_pos)
	{
		g_y_pos = 550;
	}
#endif

	if (TRUE == wnd.kbd.KeyIsPressed(VK_NUMPAD5))
	{
		shape = true;
	}
	else {
		shape = false;
	}

}

void Game::ComposeFrame()
{
	PrintReticle(300, 300, 60, 60, 60);

	if (((g_x_pos > 300-32) && (g_x_pos < 300+32)) && \
		((g_y_pos > 300 - 32) && (g_y_pos < 300 + 32)))
	{
		color_r = 50;
		color_g = 50;
		color_b = 50;

	}

	PrintReticle(g_x_pos, g_y_pos, color_r, color_g, color_b);
#if 0
	if (shape)
	{

	}
	else {
		PrintCrosshair(g_x_pos, g_y_pos, color_r, color_g, color_b);
	}
#endif

}

void Game::PrintCrosshair(int internal_offset_x, int internal_offset_y, int color_r, int color_g, int color_b)
{

	int i = 0;
	int j = 15;



	//Right line
	for (i = 0; i <= 10; i++)
	{
		gfx.PutPixel(i + internal_offset_x, j + internal_offset_y, color_r, color_g, color_b);
	}
	//Left LIne
	for (i = 20; i <= 31; i++)
	{
		gfx.PutPixel(i + internal_offset_x, j + internal_offset_y, color_r, color_g, color_b);
	}
	//Upper Line
	i = 15;
	for (j = 0; j <= 10; j++)
	{
		gfx.PutPixel(i + internal_offset_x, 0 + j + internal_offset_y + 3, color_r, color_g, color_b);
	}
	//Lower Line
	for (j = 15; j <= 25; j++)
	{
		gfx.PutPixel(i + internal_offset_x, 0 + j + internal_offset_y + 3, color_r, color_g, color_b);
	}
}

void Game::PrintReticle(int internal_offset_x, int internal_offset_y, int color_r, int color_g, int color_b)
{

	int i = 0;
	int j = 15;

	//Bigger Rectangle
	for (i = 0; i <= 31; i++)
	{
		for (j = 0; j <= 31; j++)
		{
			gfx.PutPixel(i + internal_offset_x, j + internal_offset_y, color_r, color_g, color_b);
		}
		
	}
	// Inner Void
	for (i = 2; i <= 29; i++)
	{
		for (j = 2; j <= 29; j++)
		{
			gfx.PutPixel(i + internal_offset_x, j + internal_offset_y, 0, 0, 0);
		}
	}
}

