#include <iostream>

using namespace std;

int main()
{
    int bottles_count = 10;
    cout << "There were " << bottles_count << " bottles." << endl;
    bottles_count--;
    cout << "There were " << bottles_count << " bottles." << endl;
    return 0;
}
