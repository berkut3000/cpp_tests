#include <iostream>

using namespace std;

int main()
{
    const int MAX_ATTEMPTS = 20;
    int highest = 101;
    int lowest = 1;
    int attempts = 0;
    int guess = (lowest + ((highest - lowest)*0.5));
    char user_input = '0';
    cout << "Think of a number between 1 and 100!" << endl;
    for(attempts = 1; attempts <= MAX_ATTEMPTS; attempts ++)
    {
        cout << "I guess " << guess << ". Am I right?" << endl;
        cout << "Y = Yes, H = Too high, L = Too Low, Q= Quit" << endl;
        cin >> user_input;
        if (('Y' == user_input) | ('y' == user_input))
        {
            cout << "I guessed in "<< attempts << "." << endl;
            break;
        }
        if (('H' == user_input) | ('h' == user_input))
        {
            highest = guess;
            guess = (lowest + ((highest - lowest)*0.5));
        }
        if (('L' == user_input) | ('l' == user_input))
        {
            lowest = guess;
            guess = (lowest + ((highest - lowest)*0.5));
        }

        if (('Q' == user_input) | ('q' == user_input))
        {
            cout << "You quit, bye." << endl;
            break;
        }
    }
    if(MAX_ATTEMPTS <= attempts)
    {
        cout << "Maximum number of attempts reached." << endl;
    }
    return 0;
}
