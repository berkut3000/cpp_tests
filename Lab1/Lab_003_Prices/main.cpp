#include <iostream>
#include <iomanip>
using namespace std;

#define WIDTH_FOR_COLUMNS (char)30

int main()
{
    float price_1 = 0;
    float price_2 = 0;
    float price_3 = 0;
    float total_price = 0;

    cout << "Welcome to the store!" << endl;
    cout << "Enter price for item 1: " << endl;
    cin >> price_1;
    cout << "Enter price for item 2: " << endl;
    cin >> price_2;
    cout << "Enter price for item 3: " << endl;
    cin >> price_3;

    total_price = price_1 + price_2 + price_3;

    cout << setiosflags(ios::fixed);
    cout << "Price 1 is " << setw(WIDTH_FOR_COLUMNS) << setprecision(2) <<price_1 << endl;
    cout << "Price 2 is " << setw(WIDTH_FOR_COLUMNS) << setprecision(2) << price_2 << endl;
    cout << "Price 2 is " << setw(WIDTH_FOR_COLUMNS) << setprecision(2) << price_3 << endl;
    cout << "Total Price is " << setw(WIDTH_FOR_COLUMNS-4) << setprecision(2) << total_price;

    return 0;
}
