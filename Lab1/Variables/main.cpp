#include <iostream>

using namespace std;

int main()
{
    int my_signed_int = -6;
    unsigned int my_unsigned_int = -3;

    my_signed_int *= 2;
    my_signed_int /5;

    cout << "Signed integers can be no larger than " << INT_MAX << endl;
    cout << "Signed integers can be no smaller than " << INT_MIN << endl;
    cout << "Unsigned integers can be no larger tahn " << UINT_MAX << endl;

    return 0;
}
