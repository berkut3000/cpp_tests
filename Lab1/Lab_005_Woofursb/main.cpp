#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
    int dog_count = 0;
    int dog_weight = 0;
    int dog_age = 0;

    float total_premium = 0.0;
    float base_premium = 0.0;
    float actual_premium = 0.0;

    float discount = 0.0;
    float extra_charge = 0.0;

    bool subject_to_discount = false;

    char breed_code = 0;

    int l_risk_weight = 0;

    string dog_name = "";

    cout << setiosflags(ios::fixed);
    cout << setprecision(2);

    cout << "Please enter the number of dogs in your household: ";
    cin >> dog_count;

    for (int i = 1; i<= dog_count; i++)
    {
        cout << "Enter the name of the dog #" << i << endl;
        cin.ignore();
        getline(cin, dog_name);
        cout << "Enter " << dog_name << "'s Age " << endl;
        cin >> dog_age;
        cout << "Enter " << dog_name << "'s weight " << endl;
        cin >> dog_weight;
        cout << "Enter " << dog_name << "'s breed code " << endl;
        cin >> breed_code;

        switch(breed_code)
        {
            /* Pitbull */
            case 'p':
            case 'P':
                base_premium = (dog_weight < 20) ? 30.20 : 35.15;
                subject_to_discount = false;
                cout << dog_name << " is a Pitbull" << endl;
                break;
            /* Doberman */
            case 'd':
            case 'D':
                base_premium = (dog_weight < 35) ? 28.16 : 30.00;
                subject_to_discount = true;
                cout << dog_name << " is a Doberman" << endl;
                break;
            /* Rottweiler */
            case 'r':
            case 'R':
                base_premium = (dog_weight < 45) ? 28.00 : 29.75;
                subject_to_discount = false;
                cout << dog_name << " is a Rottweiler" << endl;
                break;
            /* German Sheperd */
            case 'g':
            case 'G':
                base_premium = (dog_weight < 30) ? 27.50 : 29.75;
                subject_to_discount = true;
                cout << dog_name << " is a German Sheperd" << endl;
                break;
            /* Chow Chow */
            case 'c':
            case 'C':
                base_premium = (dog_weight < 24) ? 25.10 : 27.50;
                subject_to_discount = true;
                cout << dog_name << " is a Chow Chow" << endl;
                break;
            /* Great Dane */
            case 't':
            case 'T':
                base_premium = (dog_weight < 55) ? 25.10 : 25.20;
                subject_to_discount = true;
                cout << dog_name << " is a Great Dane" << endl;
                break;
            /* Presa Canario */
            case 's':
            case 'S':
                base_premium = (dog_weight < 45) ? 20.01 : 20.55;
                subject_to_discount = false;
                cout << dog_name << " is a Presa Canario" << endl;
                break;
            /* Akita */
            case 'k':
            case 'K':
                base_premium = 19.75;
                subject_to_discount = false;
                cout << dog_name << " is an Akita" << endl;
                break;
            /* Alaskan Malamute */
            case 'm':
            case 'M':
                base_premium = (dog_weight < 38) ? 15.50 : 18.15;
                subject_to_discount = true;
                cout << dog_name << " is an Alaskan Malamute" << endl;
                break;
            /* Husky */
            case 'h':
            case 'H':
                base_premium = (dog_weight < 20) ? 9.95 : 12.00;
                subject_to_discount = true;
                cout << dog_name << " is a Husky" << endl;
                break;
            /* Other Breed */
            case 'b':
            case 'B':
                base_premium = (dog_weight < 35) ? 4.95 : 8.95;
                subject_to_discount = true;
                cout << dog_name << " is another Breed" << endl;
                break;
            default:
                cout << "Sorry, this is not a valid breed code. Please re-enter this dog's info" << endl;
                dog_count--;
                base_premium = 0.0;
                dog_weight = 0;
                subject_to_discount = false;
                break;

        }
        if(0 < base_premium)
        {
            actual_premium = base_premium;
            if((13 < dog_age) & (true == subject_to_discount))
            {
                discount = (actual_premium * 0.20);
            }
            extra_charge = (50 < dog_weight) ? (actual_premium * .25) : 0.0;
            actual_premium = actual_premium - discount + extra_charge;
            cout << "The monthly premium for " << dog_name << " is : " << actual_premium << endl;
            total_premium += actual_premium;
            extra_charge = 0.0;
            discount = 0.0;
        }

    }
    cout << "The total monthly premium is " << total_premium << endl;
    return 0;
}
