#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    float a = 0;
    float b = 0;
    float c = 0;
    float x_1 = 0;
    float x_2 = 0;

    cout << "enter the value for a: " << endl;
    cin >> a;
    cout << "enter the value for a: " << endl;
    cin >> b;
    cout << "enter the value for a: " << endl;
    cin >> c;

    x_1 = ((-b) + sqrt((b)*(b) - (4*a*c)) )/(2*a);
    x_2 = ((-b) - sqrt((b)*(b) - (4*a*c)) )/(2*a);

    cout << "The first value for x is " << x_1 << endl;
    cout << "The second value for x is " << x_2 << endl;

    return 0;
}
