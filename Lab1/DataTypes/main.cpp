#include <iostream>

using namespace std;

int main()
{
    int num_bytes = 0;
    /* Int */
    unsigned int my_unsigned_int = UINT_MAX;
    cout << "The maximumm value our integer cand hold is " << my_unsigned_int << endl;
    num_bytes = sizeof(my_unsigned_int);
    cout << "The number of bytes required to store our integer is " << num_bytes << endl;

    /* long */
    unsigned long my_unsigned_long = ULONG_MAX;
    cout << "The maximumm value our integer cand hold is " << my_unsigned_long << endl;
    num_bytes = sizeof(my_unsigned_long);
    cout << "The number of bytes required to store our integer is " << num_bytes << endl;

    unsigned long long  my_unsigned_long_long = ULONG_LONG_MAX;
    cout << "The maximumm value our integer cand hold is " << my_unsigned_long_long << endl;
    num_bytes = sizeof(my_unsigned_long_long);
    cout << "The number of bytes required to store our integer is " << num_bytes << endl;

    unsigned short my_unsigned_short = USHRT_MAX;
    cout << "The maximumm value our integer cand hold is " << my_unsigned_short << endl;
    num_bytes = sizeof(my_unsigned_short);
    cout << "The number of bytes required to store our integer is " << num_bytes << endl;

    unsigned char my_unsigned_char = UCHAR_MAX;
    cout << "The maximumm value our integer cand hold is " << (int)my_unsigned_char << endl;
    num_bytes = sizeof(my_unsigned_char);
    cout << "The number of bytes required to store our integer is " << num_bytes << endl;

    wchar_t my_wide_char = WCHAR_MAX;
    cout << "The maximumm value our integer cand hold is " << (int)my_wide_char << endl;
    num_bytes = sizeof(my_wide_char);
    cout << "The number of bytes required to store our integer is " << num_bytes << endl;



    return 0;

}
