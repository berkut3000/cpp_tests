#include <iostream>

using namespace std;

int main()
{
    int planet_count = 8;
    bool extra_planet = true;
    int ret_val = 0;
    if (extra_planet)
    {
        planet_count++;
    }

    planet_count++;

    if ( planet_count > 9)
    {
        cerr << " solar system overflow" << endl;
        ret_val = 1;
    }
    else
    {
        cout << "Greetings, all " << planet_count << " planets. " << endl;
        ret_val = 0;
    }
    return ret_val;
}
