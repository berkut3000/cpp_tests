#include <iostream>
#include <iomanip>

using namespace std;

float process_line_item (int qty, const char *description, float price)
{
    float line_total = price * qty;

    cout << setiosflags(ios::fixed);

    cout << setw(3) << qty
            << setw(15) << description
            << setw(9) << " $" << setprecision(2) << price
            << " $" << setw(6)  << setprecision(2) << line_total <<endl;
    return line_total;
}

int main()
{
    float running_total = 0;

    cout << setw(3) << "Quantity"
            << setw(15) << "Description"
            << setw(8) << "Price"
            << setw(6) << "Total" <<endl;

    running_total += process_line_item(3, "apples", 0.45F);
    running_total += process_line_item(8, "oranges", 0.50F);
    running_total += process_line_item(1, "lobster", 21.99F);

    cout << endl;
    cout << "The grocery bill totals $ " << running_total << endl;

    return 0;
}
