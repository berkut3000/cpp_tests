#include <iostream>

using namespace std;

int main()
{
    char my_char = 'A';
    bool my_bool = true;
    bool my_other_bool = false;

    int my_converted_char = (int) my_char;
    int my_converted_bool = (int) my_bool;
    int my_converted_other_bool = (int) my_other_bool;

    char my_char_2 = (char) my_converted_char;
    bool my_bool_2 = (bool) my_converted_bool;
    bool my_other_bool_2 = (bool) my_converted_other_bool;

    float approx_pi = 3.14F;
    int sputnik_year = 1957;

    int converted_pi = (int) approx_pi;
    char converted_year = (char) sputnik_year;

    float approx_pi_2 = (float) converted_pi;
    int sputnik_year_2 = (int) converted_year;

    return 0;
}
