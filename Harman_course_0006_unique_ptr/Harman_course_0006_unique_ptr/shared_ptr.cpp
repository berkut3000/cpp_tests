//
//#include <iostream>
//#include <memory>
//
//class Recurso {
//public:
//    Recurso() { std::cout << "Recurso adquirido" << std::endl; }
//    ~Recurso() { std::cout << "Recurso liberado" << std::endl; }
//    void usar() { std::cout << "Usando recurso" << std::endl; }
//
//};
//
//void usarRecurso(std::shared_ptr<Recurso> ptr) {
//    ptr->usar();
//    std::cout << "Contador de referencias dentro de la funcion " << ptr.use_count() << std::endl;
//}
//
//int main()
//{
//    setlocale(LC_CTYPE, "Spanish");
//    std::shared_ptr<Recurso> ptr1 = std::make_shared<Recurso>();
//    std::cout << "Contador de referencias despues de creacion: " << ptr1.use_count() << std::endl;
//    {
//        std::shared_ptr<Recurso> ptr2 = ptr1;
//        std::cout << "Contador de referencia dentro del bloque: " << ptr1.use_count() << std::endl;
//        usarRecurso(ptr2);
//    }
//
//    // ptr 2 sale del �mbito ty se destruye, pero el objeto sigue vivo porque ptr1 a�n lo referencia
//    std::cout << "Contador de referencia despues de salir del bloque: " << ptr1.use_count() << std::endl;
//
//    return 0;
//}
//
//// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
//// Debug program: F5 or Debug > Start Debugging menu
//
//// Tips for Getting Started: 
////   1. Use the Solution Explorer window to add/manage files
////   2. Use the Team Explorer window to connect to source control
////   3. Use the Output window to see build output and other messages
////   4. Use the Error List window to view errors
////   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
////   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
