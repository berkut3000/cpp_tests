#include <iostream>
#include <fstream>

class Archivo {
private:
	std::ofstream archivo;

public:
	Archivo(const std::string& nombre) {
		archivo.open(nombre);
		if (archivo.is_open()) {
			std::cout << "Archivo " << nombre << "abierto." << std::endl;
		} else{
			std::cout << "No se puede abrir el archivo." << std::endl;
		}
	}

	~Archivo() {
		if (archivo.is_open()) {
			archivo.close();
			std::cout << "Archivo cerrado." << std::endl;
		}
	}

	void escribir(const std::string& texto) {
		if (archivo.is_open()) {
			archivo << texto << std::endl;
		}
	}
};

int main() {

	setlocale(LC_CTYPE, "Spanish");
	{
		Archivo miArchivo("datos.txt");
		miArchivo.escribir("Hola harman!");
	}

	return 0;
}