//
//#include <iostream>
//#include <memory>
//
////Weak ptr
//
//class Recurso {
//public:
//    Recurso() { std::cout << "Recurso adquirido" << std::endl; }
//    ~Recurso() { std::cout << "Recurso liberado" << std::endl; }
//    void usar() { std::cout << "Usando recurso" << std::endl; }
//
//};
//
//void usarRecurso(std::weak_ptr<Recurso> weak_ptr) {
//    if (auto sharedPtr = weak_ptr.lock()) {
//        sharedPtr->usar();
//        std::cout << "El recurso esta disponible." << std::endl;
//    }
//    else {
//        std::cout << "El recurso no esta disponible. " << std::endl;
//    }
//}
//
//int main()
//{
//    setlocale(LC_CTYPE, "Spanish");
//    std::shared_ptr<Recurso> ptr1 = std::make_shared<Recurso>();
//    std::weak_ptr<Recurso> weak_ptr = ptr1;
//
//    usarRecurso(weak_ptr);
//
//    ptr1.reset();
//
//    usarRecurso(weak_ptr);
//
//    return 0;
//}
//
//// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
//// Debug program: F5 or Debug > Start Debugging menu
//
//// Tips for Getting Started: 
////   1. Use the Solution Explorer window to add/manage files
////   2. Use the Team Explorer window to connect to source control
////   3. Use the Output window to see build output and other messages
////   4. Use the Error List window to view errors
////   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
////   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
