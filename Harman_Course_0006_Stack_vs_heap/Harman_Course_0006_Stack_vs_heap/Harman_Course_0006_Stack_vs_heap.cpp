// Harman_Course_0006_Stack_vs_heap.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Persona {
public:
    std::string nombre;
    Persona(const std::string& n) : nombre(n) {
        std::cout << "Constructor llamado para " << nombre << std::endl;
    }

    ~Persona() {
        std::cout << "Deconstructor llamado para " << nombre << std::endl;
    }
};

//void stackExample() {
//    int stackVar = 10; //variable en stack
//    std::cout << "Valor de stackVar: " << stackVar << std::endl;
//}
//
//void heapexample() {
//    int* heapVar = new int; //Variable en Heap
//    *heapVar = 20;
//    std::cout << "Valor de heapVar: " << *heapVar << std::endl;
//    delete heapVar;
//}


int main()
{

    Persona* pPersona = new Persona("Riacardo");
    std::cout << "Nombre: " << pPersona->nombre << std::endl;

    delete pPersona;
    //stackExample();
    //heapexample();
    //std::cout << "Hello World!\n";
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
