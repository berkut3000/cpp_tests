#include<iostream>
#include<conio.h>
#define largo 50
//SELECCION
/*
Buscar el mínimo elemento entre una posición i y el final de la lista
Intercambiar el mínimo con el elemento de la posición i
*/
using namespace std;

int seleccionsort (int A[], int n){  //Declare funcion Sort y el array A
	int min,i,j,aux;   //declaro variables y contadores...
	for (i=0; i<n-1; i++)  // compara i menor que  (n - 1), si n vale 5, se ira a la posicion 4 y asi sucesivamente...
	{	
		min=i;  //valor minimo sera 0 
		//cout<<"valor min antes: " <<min<<endl;
			for(j=i+1; j<n; j++) // si i vale 0, j vale 1 como j es menor que n, suponiendo que vale 5 sigue el bucle..
			if(A[min] > A[j])// si A[0] > A[1] 
			
			
			min=j; ///			ahora min ahora vale 1
			//cout<<"valor min despues: "<< min<<endl;
			//cout<<"valor A[min]: "<< A[min]<<endl;
			aux=A[min];	////   mi variable auxiliar... vale ahora el valor minimo
			//cout<<"valor aux: "<< aux<<endl;
			A[min]=A[i];	
		//	cout<<"valor A[min]: "<< A[min]<<endl;
		//	cout<<"valor A[i] antes: "<< A[i]<<endl;
			A[i]=aux ;
		//	cout<<"valor aux"<< aux<<endl;
			//cout<<"valor A[i] despues: "<< A[i]<<endl;
						}
			return 0;
			}
			
	int lee(int cant,int n[])  //Ingresa a la funcion Lee y declaro cant y n como array...
		{
	    int y;   // declaro y como contador
		    for(y=0;y<cant;y++)    // cant es igual al numero de datos que se tienen que ordenar dentro del array..
				{
			        cout<<"Ingresa numero "<<y+1<<": ";
					//cout<<  cant ;  //"Esto solo es para ver el valor de cant..."
					cin>>n[y];  //Asigna el dato ingresado al array en las posiciones que le tocara
			    }
			return 0;  //Regresa a 0, lo que significa que regresa a la funcion siguiente...que es seleccionsort...
		}

	int muestracadena(int cant,int n[])
		{
		    int x;
		    for(x=0;x<cant;x++)
			    {
			        cout<<n[x]<<endl;
                      /////MUESTRA LOS DATOS ORDENADOS DEL MENOR AL MAYOR...
			    }
			return 0;
		}
			
int main(int argc, char *argv[]) {  /////////AQUI EMPIEZA EL PROGRAMA!!!
	int A[largo],n;
	do{
		cout<<"Ingresa el total de numeros: ";    //Ingresa el total de numeros a ordenar
		cin>>n;
			if(n<=0||n>largo)  				 	  // Compara, tiene que ser un valor entre 0 y 50
				cout<<"Debe ingresar un valor > a 0 y < a "<<largo<<endl;  //Si no es así lo pide hasta que lo sea.. :)
	}while(n<=0||n>largo);
	
	// UNA VEZ QUE TIENE EL DATO DEL NUMERO DE DATOS A ORDENAR SE VA A LA FUNCION LEE.... que esta arriba... lalalalalalaa....
	
	lee(n,A);  // FUNCION LEEEEE
	seleccionsort(A,n);  // FUNCION SORT
	muestracadena(n,A);   // FUNCION PARA MOSTRAR ....
getch();
	}
