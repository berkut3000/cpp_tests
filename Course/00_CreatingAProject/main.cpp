#include <iostream>

using namespace std;

int main()
{
    int planet_count = 8;

    planet_count++;
    planet_count++;

    cout << "Greetings, all " << planet_count << " Planets" << endl;
    return 0;
}
